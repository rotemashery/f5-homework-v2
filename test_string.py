import requests
import pytest

#Function verifies there's no Alibaba string in page
def test_find_alibaba():
    find = 'Alibaba'
    url = 'https://the-internet.herokuapp.com/context_menu'
    response = requests.get(url)
    html = response.text
    #with pytest.raises(AssertionError):
    assert find in html
        


#Function is looking for Right-click string in page
def test_find_right_click():
    find = 'Right-click in the box below to see one called \'the-internet\''
    print(find)
    url = 'https://the-internet.herokuapp.com/context_menu'
    response = requests.get(url)
    html = response.text
    assert find in html
