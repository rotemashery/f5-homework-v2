FROM python:3.6

COPY requirements.txt /tmp/

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir --requirement /tmp/requirements.txt

ADD test_string.py /

CMD [ "python", "-m ", "pytest", "./test_string.py"]
